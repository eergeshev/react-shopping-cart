from django.contrib import admin
from eshop.models import ProductSize, ProductCategory, Product

# Register your models here.

admin.site.register(ProductSize)
admin.site.register(ProductCategory)
admin.site.register(Product)