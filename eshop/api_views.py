from rest_framework import viewsets
from . import models
from . import serializers
from .serializers import ProductSerializer
from eshop.models import Product
from rest_framework.response import Response

# class ProductViewSet(viewsets.ModelViewSet):
#     queryset = models.Product.objects.all()
#     serializer_class = serializers.ProductSerializer

# class OrderViewSet(viewsets.ModelViewSet):
#     queryset = models.Order.objects.all()
#     serializer_class = serializers.OrderSerializer

class ProductViewset(viewsets.ModelViewSet):
    serializer_class = serializers.ProductSerializer
    def list(self, request):
        queryset = Product.objects.all()
        serializer = ProductSerializer(queryset, many = True)
        return Response({'products': serializer.data})

class ProductSizeViewset(viewsets.ModelViewSet):
    queryset = models.ProductSize.objects.all()
    serializer_class = serializers.ProductSizeSerializer

class ProductCategoryViewset(viewsets.ModelViewSet):
    queryset = models.ProductCategory.objects.all()
    serializer_class = serializers.ProductCategorySerializer