
from django.contrib.auth.models import User

from rest_framework import serializers
from eshop.models import Product, ProductSize, ProductCategory

class ProductSerializer(serializers.ModelSerializer):
    availableSizes = serializers.StringRelatedField(source = 'available_sizes', many = True)
    productCategory = serializers.StringRelatedField(source = 'category')
    currencyId = serializers.CharField(source = 'currency')
    currencyFormat = serializers.CharField(source = 'currency_format')
    isFreeShipping = serializers.BooleanField(source = 'is_free_shipping')

    class Meta:
        model = Product
        fields = ('id', 'sku', 'title', 'description', 'style', 'price', 'installments','productCategory', 'currencyId', 'currencyFormat', 'isFreeShipping', 'availableSizes', 'image')


# class OrderSerializer(serializers.ModelSerializer):
   
#     class Meta:
#         model = Order
#         fields = '__all__'

class ProductSizeSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ProductSize
        fields = '__all__'

class ProductCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductCategory
        fields = '__all__'
