from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
import django.contrib.postgres.fields

from decimal import Decimal

# Create your models here.

class ProductSize(models.Model):
    name = models.CharField(max_length = 50)

    def __str__(self):
        return self.name
        
class ProductCategory(models.Model):
    name = models.CharField(max_length = 100)

    def __str__(self):
        return self.name

class Product(models.Model):
    sku              =   models.BigIntegerField(null=False)
    title            =   models.CharField(max_length = 200)
    description      =   models.TextField(max_length = 500, default='Product description.')
    style            =   models.CharField(max_length = 200)
    price            =   models.DecimalField(max_digits=12, decimal_places=2, default=Decimal('0.00'))
    installments     =   models.IntegerField()
    currency         =   models.CharField(max_length=5)
    currency_format  =   models.CharField(max_length = 2)
    is_free_shipping =   models.BooleanField()
    available_sizes  =   models.ManyToManyField(ProductSize)
    category         =   models.ForeignKey(ProductCategory, on_delete=models.SET_NULL, null=True)
    image            =   models.ImageField(blank = True, null = True)
 
    
    class Meta:
        db_table = 'product'

    

   
