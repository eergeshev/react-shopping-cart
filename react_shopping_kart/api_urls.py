from rest_framework import routers
from eshop import api_views

router = routers.DefaultRouter()
router.register(r'product',api_views.ProductViewset, basename = 'product')
# router.register(r'order', api_views.OrderViewSet)
router.register(r'size', api_views.ProductSizeViewset)
router.register(r'category', api_views.ProductCategoryViewset)